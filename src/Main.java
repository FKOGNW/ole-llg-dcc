import javax.swing.*;
import java.awt.*;

public class Main {
    public static void main (String[]args) throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        mainForm mainForm = new mainForm();
        mainForm.setVisible(true);
        mainForm.setResizable(false);
        mainForm.setMaximumSize(new Dimension(700,350));
        mainForm.setLocationRelativeTo(null);
        mainForm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
